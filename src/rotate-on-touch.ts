import gsap from 'gsap';
import { GLTF } from "three/examples/jsm/loaders/GLTFLoader";
import { onCanvasTouch } from "./utils/canvas/canvas-event.utils";


export function setRotateOnTouch(model: GLTF) {
	onCanvasTouch(() => {
		gsap.to(model.scene.rotation, {
			y: '+=.3',
			duration: 1,
			ease: 'power2.out'
		})
	});
}
