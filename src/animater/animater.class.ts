export class Animater {
    private _animationFuncitons: Record<string, Function> = {};
    private static _instanceCount = 0;
    isAnimating = false;

    constructor() {
        this._warnIfMultipleInstances();
    }

    private _warnIfMultipleInstances() {
        if (Animater._instanceCount) {
            console.warn(
                "Trying to create multiple animater objects"
            );
        }
        Animater._instanceCount++;

    }
    register(fn: Function, name: string = new Date().getMilliseconds().toString()) {
        this._animationFuncitons[name] = fn;
    }

    unRegister(name: string) {
        delete this._animationFuncitons[name];
    }

    private _runRegistered() {
        Object.values(this._animationFuncitons).map(fn => fn());
    }

    animate() {
        if (this.isAnimating)
            throw new Error(
                "Trynig to call animation loop multiple times"
            );
        this.isAnimating = true;
        this._animate();
    }

    private _animate() {
        requestAnimationFrame(this._animate.bind(this));
        this._runRegistered();
    }

}
