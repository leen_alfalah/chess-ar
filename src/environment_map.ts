
import { RGBELoader } from "three/examples/jsm/loaders/RGBELoader";
import { scene } from "./consts";
import { generator } from "./env_generator";

new RGBELoader()
    .load("empty_warehouse_01_1k.hdr", texture => {
        const envMap = generator.fromEquirectangular(texture).texture;
        generator.dispose();
        scene.environment = envMap;
    });
