import ZapparSharing from '@zappar/sharing';
import { getCanvasSnapshotURL } from '../../utils/canvas/snapshot-canvas.utils';
import { Component } from "../base/component.decorator";
import "./share-button.component.scss";


@Component({
    selector: "zapper-share-button",
    template: require("./share-button.component.html").default
})
export class ShareButtonComponent extends HTMLElement {
    openShareDialog() {
        const data = getCanvasSnapshotURL();
        ZapparSharing({ data });
    }
}