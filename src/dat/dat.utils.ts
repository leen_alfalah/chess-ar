import { GUI } from "dat.gui";
import { Object3D } from "three";
import { datGui } from "./data-gui";


export function controlPosition(obj: Object3D, name?: string): GUI {
    const folder = datGui.addFolder(name ?? obj.name);
    folder.add(obj.position, "x", 0, 10);
    folder.add(obj.position, "y", 0, 10);
    folder.add(obj.position, "z", 0, 10);
    return folder;
}

export function controlRotation(obj: Object3D, name?: string): GUI {
    const folder = datGui.addFolder(name ?? obj.name);
    folder.add(obj.rotation, "x", 0, 10);
    folder.add(obj.rotation, "y", 0, 10);
    folder.add(obj.rotation, "z", 0, 10);
    return folder;
}

export function controlScale(obj: Object3D, name?: string): GUI {
    const folder = datGui.addFolder(name ?? obj.name);
    folder.add(obj.scale, "x", 0, 10);
    folder.add(obj.scale, "y", 0, 10);
    folder.add(obj.scale, "z", 0, 10);
    return folder;
}