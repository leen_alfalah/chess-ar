import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { LoadingManager } from  "@zappar/zappar-threejs";;
export const manager = new LoadingManager();

const loader = new GLTFLoader(manager);
const PREFIX = "models";


export function loadModel(path: string) {
    const _path = [PREFIX, path].join("/");
    return loader.loadAsync(_path);
}