import { getCanvasElement } from "./get-canvas.utils";


export function getCanvasSnapshotURL(imageQuality = .8) {
    const canvas = getCanvasElement();
    const data = canvas!.toDataURL('image/jpeg', imageQuality);
    return data;
}