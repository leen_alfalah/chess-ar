import { getCanvasElement } from "./get-canvas.utils";


export function onCanvasTouch(callback: (event: { x: number, y: number }) => unknown) {
    const canvas = getCanvasElement();
    canvas.addEventListener("touchmove", (event: TouchEvent) => {
        const clientX = event.touches[0].clientX;
        const clientY = event.touches[0].clientY;
        callback({ x: clientX, y: clientY });
    })
}