export function getCanvasElement() {
    const canvas = document.querySelector("canvas");
    if (!canvas) {
        throw new Error("There should be a canvas")
    }
    return canvas;
}