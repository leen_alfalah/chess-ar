import { Camera, glContextSet, permissionDeniedUI, permissionRequestUI } from "@zappar/zappar-threejs";
import { WebGLRenderer } from "three";
import { CameraPermissionDeniedError } from "./camera-permission-denied.error";

function askForCameraPermission() {
    return permissionRequestUI()
        .then(granted => {
            if (!granted) {
                permissionDeniedUI();
                throw new CameraPermissionDeniedError();
            }
            return granted;
        });
}

export function startCameraInstanceOrThrow(camera: Camera, renderer: WebGLRenderer, front = false) {
    glContextSet(renderer.getContext());
    askForCameraPermission()
        .then(() => {
            camera.start(front);
        })


}