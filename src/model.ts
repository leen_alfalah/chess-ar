import { Camera, InstantWorldAnchorGroup, InstantWorldTracker } from "@zappar/zappar-threejs";
import { Mesh, MeshBasicMaterial, Object3D, TextureLoader } from "three";
import { animater } from "./animater/animater";
import { loadModel } from './utils/loader.utils';


function setWalkWithCamera(tracker: InstantWorldTracker) {
    animater.register(() => {
        tracker.setAnchorPoseFromCameraOffset(0, 0, -5);
    });
}

export function getInstantWorldAnchorGroup(camera: Camera) {
    const tracker = new InstantWorldTracker();
    const instantWorldAnchorGroup = new InstantWorldAnchorGroup(camera, tracker);
    setWalkWithCamera(tracker);
    return instantWorldAnchorGroup;
}

export async function loadAndSetupChessModel() {
    const model = await loadModel('chess.glb');
    model.scene.position.set(0, 0, -45);
    model.scene.traverse(async (mesh: Object3D) => {
        if (!(mesh instanceof Mesh))
            return;
        if (mesh.name.toLocaleLowerCase().includes("plane")) {
            mesh.material = new MeshBasicMaterial({
                alphaMap: new TextureLoader().load('shadow.jpg'),
                color: 0x020202,
                transparent: true,
                opacity: 0.5
            });
        }
    });
    return model;
}
