import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { animater } from "../animater/animater";
import { camera, renderer } from "../consts";


export class Control{
    private _control!: OrbitControls;
    
    
    constructor(){
        this._setupControl();
        this._registerOnAnimater();
    }

    private _setupControl(){
        this._control = new OrbitControls(camera, renderer.domElement);
        camera.position.set(-21, 34, 4);
        this._control.enableDamping = true
        this._control.dampingFactor = .9;
        this._control.keys = {
            LEFT: "ArrowLeft", //left arrow
            UP: "ArrowUp", // up arrow
            RIGHT: "ArrowRight", // right arrow
            BOTTOM: "ArrowDown" // down arrow
        }
        this._control.listenToKeyEvents(document.body);
    }

    private _registerOnAnimater(){
        animater.register(() => {
            this._control.update();
        });
    }
}