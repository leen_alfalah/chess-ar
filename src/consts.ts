import { Camera } from "@zappar/zappar-threejs";
import { Scene, WebGLRenderer } from "three";
import { getCanvasElement } from "./utils/canvas/get-canvas.utils";


export const camera = new Camera();

export const scene = new Scene();

export const renderer = new WebGLRenderer({
    alpha: true,
    antialias: true,
    preserveDrawingBuffer: true,
    canvas: getCanvasElement()
});