import { animater } from "./animater/animater";
import { camera, scene } from "./consts";
import { controlScale } from './dat/dat.utils';
import "./environment_map";
import { getInstantWorldAnchorGroup, loadAndSetupChessModel } from "./model";
import { setRotateOnTouch } from "./rotate-on-touch";
import { setup } from "./setup";


setup();


const instantWorldAnchorGroup = getInstantWorldAnchorGroup(camera);
scene.add(instantWorldAnchorGroup);

const model = await loadAndSetupChessModel();
instantWorldAnchorGroup.add(model.scene);
setRotateOnTouch(model);
controlScale(model.scene, 'chessBoard');

animater.animate();
