import { ACESFilmicToneMapping, sRGBEncoding } from "three";
import { animater } from "./animater/animater";
import { camera, renderer, scene } from "./consts";
import { startCameraInstanceOrThrow } from "./utils/camera.utils";


/**
 * @description handles the whole process to spin up the scene\n
 * - resizing the canvas
 * - ask for camera permission
 * - setup the camera as scene background
 * - register frame updates functions
 */
export function setup() {
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true;
    renderer.toneMapping = ACESFilmicToneMapping;
    renderer.toneMappingExposure = 1;
    renderer.outputEncoding = sRGBEncoding;

    startCameraInstanceOrThrow(camera, renderer, false);
    scene.background = camera.backgroundTexture;
    animater.register(() => {
        camera.updateFrame(renderer);
        renderer.render(scene, camera);
    });
}